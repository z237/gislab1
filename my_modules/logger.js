var winston = require('winston')
var congif = require('config')

var logger = new winston.Logger({
	transports: [
		new (winston.transports.Console)(colorize: true),
		new (winston.transports.File(filename: config.get('logPath'))
	]
})